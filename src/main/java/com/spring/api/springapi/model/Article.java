package com.spring.api.springapi.model;

import org.springframework.lang.Nullable;

public class Article {
    public int id;
    public String title;
    public String description;
    public String img;
    public Article(){

    }
    public Article(int id, String title, String description, @Nullable  String img) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.img = img;
    }
    public String getImg() {
        if (img == null) return null;

        return "/articles/" + img;
    }
}
