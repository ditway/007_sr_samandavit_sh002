package com.spring.api.springapi.controller;

import com.spring.api.springapi.model.Article;
import com.spring.api.springapi.util.FileUploadUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RootController {

    List<Article> articles;
    private final String imgDir = "articles";

    RootController() {
        articles = new ArrayList<>();
        articles.add(new Article(1, "Ant and the Man", "The Ant Man", "haha4.jpg"));
        articles.add(new Article(2, "Art of War", "The world war 2", "haha4.jpg"));
        articles.add(new Article(3, "Avenger end game", "Marvel Avenger", "haha4.jpg"));
    }

    public int genId() {
        return articles.stream().mapToInt(x -> x.id).max().getAsInt() + 1;
    }

    @GetMapping("/")
    public String home(Model model) {
        model.addAttribute("articles", articles);
        return "home-page";
    }

    @GetMapping("/create")
    public String createForm(Model model) {

        model.addAttribute("article", new Article());
        return "create-article";
    }

    @PostMapping("/create")
    public String create(@RequestParam("title") String title, @RequestParam("file") MultipartFile file,
                         @RequestParam("description") String description) throws IOException {
        try {
            if (!file.isEmpty()) {
                var filename = StringUtils.cleanPath(file.getOriginalFilename());
                articles.add(new Article(genId(), title, description, filename));
                FileUploadUtil.saveFile(imgDir, filename, file);
            } else
                articles.add(new Article(genId(), title, description, null));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String editForm(Model model, @PathVariable String id) {
        var optional = articles.stream().filter(x -> x.id == Integer.parseInt(id)).findFirst();

        if (optional.isPresent())
            model.addAttribute("article", optional.get());

        return "create-article";
    }
    @GetMapping("/view/{id}")
    public String view(Model model, @PathVariable String id) {
        var optional = articles.stream().filter(x -> x.id == Integer.parseInt(id)).findFirst();

        if (optional.isPresent())
            model.addAttribute("article", optional.get());

        return "view-article";
    }

    @PostMapping("/edit/{id}")
    public String edit(@PathVariable String id, @RequestParam("title")
            String title, @RequestParam("file") MultipartFile file, @RequestParam("description") String description) throws IOException {
        var optional = articles.stream().filter(x -> x.id == Integer.parseInt(id)).findFirst();
        if (optional.isPresent()) {
            var article = optional.get();
            article.title = title;
            article.description = description;
            if (!file.isEmpty()) {
                var filename = StringUtils.cleanPath(file.getOriginalFilename());
                article.img = filename;
                FileUploadUtil.saveFile(imgDir, filename, file);
            }
        }

        return "redirect:/";
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        var optional = articles.stream().filter(x -> x.id == Integer.parseInt(id)).findFirst();
        if (optional.isPresent()) {
            var article = optional.get();
            articles.remove(article);
        }
        return "redirect:/";
    }
}
